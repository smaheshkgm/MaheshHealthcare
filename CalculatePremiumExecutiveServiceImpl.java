package CalculatePremiumServiceImpl;

import emids.healthInsurance.model.DailyHabits;
import emids.healthInsurance.model.Health;
import emids.healthInsurance.model.Person;
import emids.healthInsurance.service.CalculatePremiumService;

public class CalculatePremiumExecutiveServiceImpl implements CalculatePremiumService{
	//Should read ALL these rules from Config/Properties file(using a singleton class)to make it more flexible.
		public double getPremium(Person person) {
			double premium = getBasePremium();
			
			premium += premium * getPremiumPercentageBasedOnAgeGender(person) / 100;
			premium += premium * getPremiumPercentageBasedOnHabits(person) / 100;
			premium += premium * getPremiumPercentageBasedOnHealth(person) / 100;
			
			// some discount for Executive Customers plus some other business calculations
			premium = premium * 0.9;
			
			return premium;
		}
		
		protected float getBasePremium() {
			return 5000; //read from config/Properties file.
		}
		
		protected float getPremiumPercentageBasedOnAgeGender(Person person) {
			float percentage = (float)0.0;
			/* case: age < 18 and age > 100 might have to be handled by throwing some Business Exception*/
			if(person.getAge() > 40)
				percentage += (float) 10.0 * ((person.getAge()-41)/5 + 1);
			if(person.getAge() > 35)
				percentage += (float) 10.0;
			if(person.getAge() > 30)
				percentage = (float)10.0;
			if(person.getAge() > 25)
				percentage += (float) 10.0;
			if(person.getAge() > 18)
				percentage += (float) 10.0;
			
			
			if(person.getGender() == 'M')
				percentage += (percentage/10) * 2;
			return percentage;
		}
		
		protected float getPremiumPercentageBasedOnHabits(Person person) {
			float percentage = (float)0.0;
			DailyHabits habits = person.getHabits();
			if(habits.isAlcohol())
				percentage += (float) 3.0;
			if(habits.isDailyExcersie())
				percentage += (float) -3.0;
			if(habits.isDrugs())
				percentage += (float) 3.0;
			if(habits.isSmoking())
				percentage += (float) 3.0;
			return percentage;
		}
		
		protected float getPremiumPercentageBasedOnHealth(Person person) {
			float percentage = (float)0.0;
			Health health = person.getHealth();
			if(health.isBloodPressure())
				percentage += (float) 1;
			if(health.isBloodSugar())
				percentage += (float) 1;
			if(health.isHyperTension())
				percentage += (float) 1;
			if(health.isOverweight())
				percentage += (float) 1;
			return percentage;
		}
}

