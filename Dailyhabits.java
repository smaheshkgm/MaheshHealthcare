package emids.healthInsurance.model;

public class DailyHabits {
	private boolean smoking;
	private boolean Alcohol;
	private boolean dailyExcersie;
	private boolean drugs;
	
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return Alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		Alcohol = alcohol;
	}
	public boolean isDailyExcersie() {
		return dailyExcersie;
	}
	public void setDailyExcersie(boolean dailyExcersie) {
		this.dailyExcersie = dailyExcersie;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
}
