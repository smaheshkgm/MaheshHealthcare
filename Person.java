package emids.healthInsurance.model;

public class Person {
	private long id;
	private String name;
	private int age;
	private char gender;
	
	private Health health;
	
	private DailyHabits habits;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Health getHealth() {
		return health;
	}

	public void setHealth(Health health) {
		this.health = health;
	}

	public DailyHabits getHabits() {
		return habits;
	}

	public void setHabits(DailyHabits habits) {
		this.habits = habits;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}
}
