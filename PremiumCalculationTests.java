package emids.healthInsurance.UTests;
import org.junit.Test;

import CalculatePremiumServiceImpl.CalculatePremiumExecutiveServiceImpl;
import CalculatePremiumServiceImpl.CalculatePremiumStandardServiceImpl;
import CalculatePremiumServiceImpl.PremiumCalculation;
import emids.healthInsurance.model.DailyHabits;
import emids.healthInsurance.model.Health;
import emids.healthInsurance.model.Person;
import emids.healthInsurance.service.CalculatePremiumService;

import static junit.framework.Assert.assertEquals;


public class PremiumCalculationTests {

	private PremiumCalculation premiumCalculation;
	
	@Test
	public void TestPremiumCalculation() {
		premiumCalculation = new PremiumCalculation();// bean
		CalculatePremiumService standardService = new CalculatePremiumStandardServiceImpl();
		Person person = createSamplePerson();		
		assertEquals((double)6856, premiumCalculation.calculate(standardService, person));
		
		CalculatePremiumService executiveService = new CalculatePremiumExecutiveServiceImpl();
		assertEquals((double)6856, premiumCalculation.calculate(executiveService, person));
		
	}
	
	/*@Test
	public void TestStandardPremiumCalculation() {
		CalculatePremiumService service = new CalculatePremiumStandardServiceImpl();
		Person person = createSamplePerson();
		premiumCalculation = new PremiumCalculation( service, person);// bean		
		assertEquals((double)6856, premiumCalculation.calculate());
		
	}
	
	@Test
	public void TestExecutivePremiumCalculation() {
		CalculatePremiumService service = new CalculatePremiumExecutiveServiceImpl(); // some discount for executive customers.
		Person person = createSamplePerson();
		premiumCalculation = new PremiumCalculation( service, person);
		assertEquals((double)6856, premiumCalculation.calculate());
		
	}*/
	
	private Person createSamplePerson() {
		Person person = new Person();
		person.setName("Norman Gomes");
		person.setAge(34);
		person.setGender('M');
		
		Health health = new Health();
		health.setHyperTension(false);
		health.setBloodSugar(false);
		health.setHyperTension(false);
		health.setOverweight(true);
		
		DailyHabits habits = new DailyHabits();
		habits.setSmoking(false);
		habits.setAlcohol(true);
		habits.setDailyExcersie(true);
		habits.setDrugs(false);
		
		person.setHealth(health);
		person.setHabits(habits);
		return person;
	}
}
