package CalculatePremiumServiceImpl;

import emids.healthInsurance.model.Person;
import emids.healthInsurance.service.CalculatePremiumService;

public class PremiumCalculation {
	public double calculate(CalculatePremiumService service, Person person) {
		return service.getPremium(person);
	}
}
