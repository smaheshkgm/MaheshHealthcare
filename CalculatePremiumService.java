package emids.healthInsurance.service;

import emids.healthInsurance.model.Person;

public interface CalculatePremiumService {
	double getPremium(Person person);
}
